/**
 * 
 */
package yahooMailPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author User
 *
 */
public class YahooLoginPage2 {
	
	WebDriver driver;
	
	public YahooLoginPage2(WebDriver driver)
	{
		
		this.driver = driver;
	}
	
	By passwordField = By.id("login-passwd");
	By signinButton = By.id("login-signin");
	
	public void typePassword(String pwd)
	{
		WebElement password = null;
		
		while(true)
			{
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			password = driver.findElement(passwordField);
			
			
			if(password.isDisplayed())
				break;
			else
				continue;
		}
		
		password.sendKeys(pwd);
		
	}
	
	public void hitSignIn()
	{
		WebElement signIn= null; 
		while(true)
		{
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			signIn = driver.findElement(signinButton);
			
			if(signIn.isDisplayed())
				break;
			else
				continue;
		}
		signIn.click();

     }
}
