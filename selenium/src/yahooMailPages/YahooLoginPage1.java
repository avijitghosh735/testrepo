/**
 * 
 */
package yahooMailPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author User
 *
 */
public class YahooLoginPage1 {

	WebDriver driver;
	
	public YahooLoginPage1(WebDriver driver)
	{
	 this.driver = driver;
	}
	

	By userNameField = By.id("login-username");
	By next = By.id("login-signin");
	
	
	public void typeUserName(String user) throws InterruptedException
	
	{
		WebElement username = null;
		while(true)
			{
			Thread.sleep(2000);
			username = driver.findElement(userNameField);
			if(username.isDisplayed())
				break;
			else
				continue;
		}
		username.sendKeys(user);
		
	}
	
	public void hitNextButton() throws InterruptedException
	{
		WebElement nextButton = null;
		while(true)
		{
			Thread.sleep(2000);
			nextButton = driver.findElement(next);
			if(nextButton.isDisplayed())
				break;
			else
				continue;
		}
		nextButton.click();
	}
	
}
