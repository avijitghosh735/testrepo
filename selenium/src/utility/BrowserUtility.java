/**
 * 
 */
package utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * @author User
 *
 */
public class BrowserUtility {
	
	public static WebDriver getDriverForBrowser(String browserName)
	{
		WebDriver driver = null;
		
		if(browserName.equalsIgnoreCase("Firefox"))
				{
			driver = new FirefoxDriver();
				}
		
		if(browserName.equalsIgnoreCase("Chrome"))
		{
			
			driver =  new ChromeDriver();
		}
		
		return driver;
		
	}

}
