/**
 * 
 */
package naukri.multipleWindows;

import java.util.ArrayList;
import java.util.Set;

import org.openqa.selenium.WebDriver;

/**
 * @author User
 *
 */
public class NaukriPage {
	
	WebDriver driver;
	String parentHandle;
	Set<String> windowHandles;
	ArrayList<String> windowHandlesList; //= new ArrayList<>(windowHandles);
	
	
	public NaukriPage(WebDriver driver,String url)
	{
		this.driver=driver;
		/*try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.switchTo().alert().dismiss();*/
		this.driver.manage().window().maximize();
		this.driver.get(url);
		parentHandle=this.driver.getWindowHandle();
		System.out.println("Parent Window Id:"+parentHandle);
		windowHandles=this.driver.getWindowHandles();
		windowHandlesList = new ArrayList<>(windowHandles);
	}
	
	public void switchToWindow(int nextWindowNo)
	
	{
		
		
		driver.switchTo().window(windowHandlesList.get(nextWindowNo));
		
		if(nextWindowNo!=0)
			driver.manage().window().maximize();
			
		
	}
	
	public void showTitleAndUrl()
	{
		//driver.switchTo().alert().dismiss();
		Set<String> ls = driver.getWindowHandles();
		System.out.println("Currently the no of window open is:"+ls.size());
		System.out.println("Current window tile:"+" "+driver.getTitle());
		System.out.println("Current window url:"+" "+driver.getCurrentUrl());
		System.out.println("Test passed!");
	}
	
	public void closePopUp()
	{
		System.out.println("Going to close window with title and window handle:"+" "+driver.getTitle()+"   "+driver.getWindowHandle());
		driver.close();
	}
			

}
