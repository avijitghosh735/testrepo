package naukri.multipleWindows;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
//import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class NaukriPopUpHandleTest {
	
	@Test
	public void multipleWindowTestMethod() throws InterruptedException
	//public static void main(String[] args) throws InterruptedException
	{
	
	System.setProperty("webdriver.chrome.driver","/C:/chromedriver/chromedriver.exe");
	ChromeOptions options = new ChromeOptions();
	options.addArguments("--disable-extensions");
	//WebDriver driver = new ChromeDriver(options);
	//WebDriver driver=new ChromeDriver();
	NaukriPage naukriPage = new NaukriPage(new ChromeDriver(options), "https:\\www.naukri.com");
	Thread.sleep(2000);
	naukriPage.switchToWindow(1);
	Thread.sleep(2000);
	naukriPage.closePopUp();
	
	naukriPage.switchToWindow(2);
	Thread.sleep(2000);
	naukriPage.closePopUp();
	//Closing Window3
	naukriPage.switchToWindow(3);
	Thread.sleep(2000);
	naukriPage.closePopUp();
	
	/*naukriPage.switchToWindow(3);
	Thread.sleep(3000);
	naukriPage.closePopUp();*/
	
	naukriPage.switchToWindow(0);
	Thread.sleep(2000);
	naukriPage.showTitleAndUrl();
	
	}
	

}
