import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * 
 */

/**
 * @author User
 *
 */
public class WebTableExample {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		//WebDriver driver = new ChromeDriver();
		System.setProperty("webdriver.chrome.driver","/C:/chromedriver/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-extensions");
		WebDriver driver = new ChromeDriver(options);
		driver.get("https://www.w3schools.com/html/html_tables.asp");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		List<WebElement> e = driver.findElements(By.xpath("//table[@id='customers']/tbody/tr"));
		//List<WebElement> eList = (List<WebElement>) e;
		int noOfRows=e.size()-1;
		System.out.println("No of rows:"+ noOfRows);
		System.out.println(e.get(1).getText());
		String stringSelector;
		for(int i=1;i<=noOfRows;i++)
		{
			stringSelector=e.get(i).getText();
			if(stringSelector.endsWith("Canada"))
					{
					System.out.println("Details of Canada customer:");
					System.out.println(e.get(0).getText());
					System.out.println(stringSelector);
					}
		}

	}

}
