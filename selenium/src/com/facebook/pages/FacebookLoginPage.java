/**
 * 
 */
package com.facebook.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author User
 *
 */
public class FacebookLoginPage {
	
	WebDriver driver;
	
	public FacebookLoginPage(WebDriver driver)
	{
		this.driver = driver;
	}
	
	@FindBy(id="email")
	WebElement email;
	
	@FindBy(how=How.ID, using="pass")
	WebElement password;	
	
	@FindBy(how=How.XPATH, using = ".//*[@id='loginbutton']")
	WebElement loginButton;
	
	public void typeEmail(String email)
	{
		this.email.sendKeys(email);
	}
	
	public void typePassword(String password)
	{
		this.password.sendKeys(password);
	}
	
	public void hitLoginButton()
	{
		loginButton.click();
	}

//Add 1 comment
	//Add 2nd comment
	//this.password.sendKeys(password);

//Add comment A
	//Add comment B
	//loginButton.click();

}
