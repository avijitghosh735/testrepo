package selenium;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;

public class DynamicBrowsers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
						
				//String browserName;
		
		//Scanner in = new Scanner(System.in);
		
		
		//browserName = in.nextLine();
		//String browserName = System.console().readLine();
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a browser name");
	    String browserName = null;
	    try {
	        browserName = reader.readLine();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    
	    System.out.println("You entered: "+browserName);
	    System.out.println("Length of the entered string = "+ browserName.length());
	    
		
		
				
					
		   
				WebDriver driver = null;// WebDriver is an interface in Java- diff driver classes implements it
				
				if(browserName.compareTo("Chrome") == 0)
				{
					System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\chromedriver\\chromedriver.exe");
					driver = new ChromeDriver();//ChromeDriver is a driver class that implements interface WebDriver
					 driver.get("https://mail.yahoo.com");//loads a web page in the browser
					 System.out.println(driver.getTitle());//Prints the title of the web page
					 WebElement user = driver.findElement(By.id("login-username"));
				        //CharSequence[] i = {"zealot.1948"};
				        //CharSequence[] j = {"bubai0085"};
				        user.sendKeys("zealot.1948");
				        WebElement pwd = driver.findElement(By.id("login-passwd"));
				        pwd.sendKeys("bubai0085");
				        WebElement signInButton = driver.findElement(By.id("login-signin"));
				        signInButton.click();
				    }
					 
					 //WebElement userId = driver.findElement(By.id("))
					 
				//}
				else if(browserName.compareTo("Mozilla") == 0)
						{
					System.out.println(browserName);
					
					ProfilesIni allProf = new ProfilesIni();// represents an object with all firefox profiles in the system
					FirefoxProfile myProf = allProf.getProfile("selenium");//returns a particular firefox profile
					driver = new FirefoxDriver(myProf);// FirefoxDriver is a driver class that implements interface WebDriver- here we are using FirefoxDriver as constructor- to open a particular profile of Firefox, present in system
					driver.get("https://mail.yahoo.com");//loads a web page in the browser
					System.out.println(driver.getTitle());//Prints the title of the web page
					//WebElement user = driver.findElement(By.id("login-username"));// finding element by id
					WebElement user = driver.findElement(By.xpath("//input[@id = 'login-username']"));//finding element by partial xpath
					//CharSequence[] i = {"zealot.1948"};
			        //CharSequence[] j = {"bubai0085"};
			        user.sendKeys("zealot.1948");
			        //WebElement pwd = driver.findElement(By.id("login-passwd"));
			        WebElement pwd = driver.findElement(By.xpath("//input[@id='login-passwd']"));//finding element by partial xpath
					pwd.sendKeys("bubai0085");
			        //WebElement signInButton = driver.findElement(By.id("login-signin"));
			        WebElement signInButton = driver.findElement(By.xpath("//button[@id='login-signin']"));//finding element by partial xpath
					signInButton.click();
			    }
				//else if(browserName == "Internet Explorer")
				
			}

		}


	
